<?php
/**
 * @file
 *   Shift Admin Callbacks and Controller Class
 */


/**
 * @class ShiftTypeUIController
 */
class ShiftTypeUIController extends EntityDefaultUIController {

  /**
   * Extends EntityDefaultUIController::hook_menu().
   *
   * If you want a deeper menu structure for your entity type, then
   * you probably need to define and override the paths here.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = 'Shift Types';
    $items[$this->path]['description'] = 'Manage shift types and fields for shifts.';
    return $items;
  }
}


/**
 * Shift Type Form.
 *
 * @param $form
 * @param &$form_state
 * @param $type
 * @param $op
 * @return array form array
 */
function shift_type_form($form, &$form_state, $type, $op = 'edit') {

  if ($op == 'clone') {
    $type->label .= ' (clone)';
    $type->name .= '_clone';
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The human-readable name for this shift type'),
    '#default_value' => isset($type) ? $type->label : '',
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  $form['name'] = array(
    '#type' => 'machine_name', // See form api docs on api.drupal.org
    '#title' => t('Name'),
    '#maxlength' => 100,
    '#machine_name' => array(
      'exists' => 'shift_type_exists',
      'source' => array('label'),
    ),
    '#default_value' => isset($type->name) ? $type->name : '',
  );

  if (isset($type->id)) {
    $settings = unserialize($type->settings);
  }

  $form['settings'] = array('#tree' => TRUE);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  if (isset($type->id)) {
    $form_state['type'] = $type;

    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('shift_type_form_delete'),
      '#weight' => 41,
    );
  }

  return $form;
}

/**
 * Shift Type Form delete callback that redirects to Entity API's delete page
 * callback.
 *
 * @param $form
 * @param &$form_state
 */
function shift_type_form_delete($form, &$form_state) {
  $form_state['redirect'] = array('admin/structure/shift/manage/' . $form_state['type']->name . '/delete');
}

/**
 * Shift type form submit callback
 *
 * @param $form
 * @param &$form_state
 */
function shift_type_form_submit($form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->settings = serialize($form_state['values']['settings']);
  $type->save();
  $form_state['redirect'] = array('admin/structure/shift');
}

