<?php
/**
 * @file
 *   Shift page callbacks and forms
 */


/**
 * Shift Entity Form.
 * 
 * @param $form
 * @param &$form_state
 * @param $shift
 *   An optional shift object or an array of one shift object.
 * @param $op
 *   The operation to do
 * @return array a form array
 */
function shift_form($form, &$form_state, $shift = NULL, $op = 'edit') {

  $form['actions'] = array('#type' => 'actions');

  if (!isset($shift) && !isset($form_state['shift'])) {
    // If we do not have a shift object, then we need to load the type selector.
    $types = shift_get_types();
    
    if (empty($types)) {
      // If we do not have any types, display a message and return.
      drupal_set_message(t('There are no shift types configured yet. Please configure a shift type first.'), 'error');
      return $form;
    }

    foreach ($types as $name => $type) {
      $type_options[$name] = $type->label;
    }

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Shift type'),
      '#description' => t('Select a shift type to add.'),
      '#options' => $type_options,
      '#required' => TRUE,
    );

    $form['actions']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#submit' => array('shift_form_next'),
    );

    $form['#validate'] = array('shift_form_next_validate');
    return $form;
  }
  else if (isset($form_state['shift'])) {
    $shift = $form_state['shift'];
  }
  else if (is_array($shift)) {
    $shift = array_pop($shift);
    $form_state['shift'] = $shift;
  }

  $form['uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Shift User'),
    '#description' => t('Provide the user associated with this shift.'),
    '#default_value' => isset($shift->uid) ? user_load($shift->uid)->name : '',
    '#autocomplete_path' => 'user/autocomplete',
    '#required' => TRUE,
  );

  $form['start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Shift start'),
    '#description' => t('Choose the time and date when the shift began.'),
    '#date_format' => 'm/d/Y H:i:s',
    '#date_increment' => 15,
    '#default_value' => isset($shift->start) ? date('Y-m-d H:i:s', $shift->start) : date('Y-m-d H:i:s'),
    '#required' => TRUE,
  );

   $form['stop'] = array(
    '#type' => 'date_popup',
    '#title' => t('Shift stop'),
    '#description' => t('Choose the time and date when the shift ended.'),
    '#date_format' => 'm/d/Y H:i:s',
    '#date_increment' => 15,
    '#default_value' => isset($shift->stop) ? date('Y-m-d H:i:s', $shift->stop) : '',
  );

  // Attach our field widgets to our form.
  field_attach_form('shift', $shift, $form, $form_state); 

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  if (isset($shift->sid) && $op == 'edit' && user_access('administer shift')) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('shift_form_delete'),
      '#weight' => 41,
    );
  }

  return $form;
}

/**
 * Shift form validation.
 *
 * @param $form
 * @param &$form_state
 */
function shift_form_validate($form, &$form_state) {

  // Check if the user exists and is not anonymous.
  $account = user_load_by_name($form_state['values']['uid']);
  if (!$account) {
    form_set_error('uid', t('A valid user is required for a shift.'));
  }
  else {
    $form_state['shift']->uid = $account->uid;
    $form_state['values']['uid'] = $account->uid;
  }

  // Validate our date/time strings as actual datetime.
  $start = DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['start']);
  if (!$start) {
    form_set_error('start', t('Invalid date/time entry: %date', array('%date' => $form_state['values']['start'])));
  }
  else {
    $form_state['values']['start'] = $start->format('U');
  }

  $stop = !empty($form_state['values']['stop']) ? DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['stop']) : NULL;
  if (isset($stop) && !$stop) {
    form_set_error('stop', t('Invalid date/time entry.'));
  }
  else if (isset($stop) && $start > $stop) {
    form_set_error('stop', t('A shift cannot stop before it begins.'));
  }
  else if (isset($stop)) {
    $form_state['values']['stop'] = $stop->format('U');
  }
  else {
    $form_state['values']['stop'] = NULL;
  }

  // validate our fields.
  field_attach_form_validate('shift', $form_state['shift'], $form, $form_state);
}

/**
 * Shift form next validate. Empty function.
 */
function shift_form_next_validate($form, &$form_state) {

}

/**
 * Shift form next submit callback.
 *
 * @param $form
 * @param &$form_state
 */
function shift_form_next($form, &$form_state) {
  $form_state['shift'] = shift_create(array('type' => $form_state['values']['type']));
  $form_state['rebuild'] = TRUE;
}

/**
 * Shift form submit.
 *
 * @param $form
 * @param &$form_state
 */
function shift_form_submit($form, &$form_state) {
  $shift = $form_state['shift'];

  // Setup our dates.
  $start = DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['start']);
  $stop = DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['stop']);

  $shift->start = $form_state['values']['start'];
  $shift->stop = $form_state['values']['stop'];

  // Submit to the field api.
  field_attach_submit('shift', $shift, $form, $form_state);

  $shift = shift_save($shift);

  $form_state['redirect'] = array('admin/shift');
}

/**
 * Shift form delete submit callback.
 *
 * @param $form 
 * @param &$form_state
 */
function shift_form_delete($form, &$form_state) {
  $form_state['redirect'] = array('admin/shift/' . $form_state['shift']->sid . '/delete');
}

/**
 * Shift delete form.
 *
 * @param $form
 * @param &$form_state
 * @param $shift
 * @return array
 */
function shift_delete_form($form, &$form_state, $shift) {
 
  // Pop the end off of the array.
  if (is_array($shift)) {
    $shift = array_pop($shift);
  }
  $form_state['shift'] = $shift;

  // Load up the type
  $type = shift_type_load($shift->type);

  // Make a friendly name.
  $form_state['user'] = user_load($shift->uid);
  $name = format_username($form_state['user']);

  // mradcliffe's magical help message on forms.
  $form['help'] = array(
    '#prefix' => '<div class="messages warning">',
    '#markup' => t("Are you sure you want to <em>permanently</em> delete %name's @type shift beginning at @date?", array('%name' => $name, '@type' => $type->label, '@date' => format_date($shift->start, 'short'))),
    '#suffix' => '</div>',
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm delete'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Shift delete form submit.
 *
 * @param $form
 * @param &$form_state
 */
function shift_delete_form_submit($form, &$form_state) {
  $shift = $form_state['shift'];
  $account = $form_state['user'];

  $sid = $shift->sid;
  $type = $shift->type;
  $start = $shift->start;

  shift_delete($shift);

  watchdog('shift', 'Shift @sid was permanently deleted.', array('@sid' => $sid), WATCHDOG_INFO);
  drupal_set_message(t('Successfully deleted shift @sid.', array('@sid' => $sid)));

  $form_state['redirect'] = array('admin/shift');
}
