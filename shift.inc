<?php
/**
 * @file
 *  Shift entity and Shift Type entity class definitions.
 */


/**
 * @class ShiftEntity
 */
class ShiftEntity extends EntityDB {

  /**
   * Shift Constructor.
   *
   * In reality you do not need any of the setters defined in this class as
   * Entity will do it for you EXCEPT your bundle property if your entity
   * type supports bundles.
   *
   * @param $values an array of values to set.
   */
  public function __construct($values = array()) {

    if (isset($values['sid'])) {
      $this->setShiftId($values['sid']);
      unset($values['sid']);
    }

    if (isset($values['vid'])) {
      $this->setShiftRevision($values['vid']);
      unset($values['vid']);
    }

    if (isset($values['type'])) {
      $this->setShiftType($values['type']);
      unset($values['shift']);
    }

    // It's easy to forget that $values comes first here.
    parent::__construct($values, 'shift');
  }

  /**
   * Shift id Setter.
   *
   * @param $sid
   */
  public function setShiftId($sid) {
    $this->sid = $sid;
  }

  /**
   * Shift type setter.
   *
   * @param $type
   */
  public function setShiftType($type) {
    $this->type = $type;
  }

  /**
   * Set the duration from the start and stop if stop exists.
   */
  public function setDuration() {
    if (!empty($this->stop)) {
      $this->duration = $this->stop - $this->start;
    }
  }

  /**
   * Save method, which calls Entity Controller save method.
   */
  public function save() {
    global $user;

    // Set duration
    $this->setDuration();

    // Set revision properties.
    $this->is_new_revision = TRUE;
    $this->changed = time();
    $this->changedby = $user->uid;

    parent::save();
  }

}


/**
 * @class ShiftTypeEntity
 */
class ShiftTypeEntity extends EntityDB {

  public $name;
  public $label;

  /**
   * Shift Type Constructor
   * @params $values
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'shift_type');
  }

}
