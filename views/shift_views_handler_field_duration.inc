<?php
/**
 * @file
 *   Shift handler for duration.
 */


/**
 * @class shift_views_handler_field_duration
 */
class shift_views_handler_field_duration extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    $options['format'] = array('default' => 'short', 'translatable' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['format'] = array(
      '#type' => 'select',
      '#title' => t('Duration format'),
      '#description' => t('Select the format to display the shift duration. Raw is the raw value in seconds. Short is displayed as <em>00:00:00</em>. Long is displayed as <em>0 hours 0 minutes 0 seconds</em>'),
      '#options' => array('raw' => t('Raw value'), 'short' => t('Short'), 'long' => t('Long')),
      '#default_value' => $this->options['format'],
    );

    parent::options_form($form, $form_state);
  }

  function render($values) {
    $duration = $this->get_value($values);

    if (empty($duration)) {
      return '';
    }

    $attributes = array(
      'class' => array('shift-duration', 'shift-duration-' . $this->options['format']),
    );

    return theme('shift_duration', array('duration' => $duration, 'format' => $this->options['format'], 'attributes' => $attributes));
  }

}
