<?php
/**
 * @file
 *   Shift views handler for operations. This provides an extra
 *   views field that creates operation links. Very handy.
 */


/**
 * @class shift_views_handler_field_operations
 */
class shift_views_handler_field_operations extends views_handler_field {

  function query() {
    // We actually need to query for sid to build our operations.
    $this->real_field = 'sid';

    parent::query();
  }

  function render($values) {
    $sid = $this->get_value($values);

    // Setup links based on user_access().
    $links = array(
      'shift-edit' => array(
        'title' => t('Edit'),
        'href' => 'admin/shift/' . $sid . '/edit',
      ),
    );

    if (user_access('administer shift')) {
      $links['shift-delete'] = array(
        'title' => t('Delete'),
        'href' => 'admin/shift/' . $sid . '/delete',
      );
    }

    // Setup a variables for theme function
    $vars = array(
      'links' => $links,
      'attributes' => array(
        'class' => array('links', 'inline', 'shift-operations'),
      ),
    );

    return theme('links', $vars);
  }

}
