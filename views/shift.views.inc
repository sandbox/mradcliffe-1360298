<?php
/**
 * @file
 *  Shift views callbacks and ShiftViewsController.
 */

/**
 * @class ShiftViewsController
 */
class ShiftViewsController extends EntityDefaultViewsController {
  public function views_data() {
    $data = parent::views_data();

    $data['shift']['operations'] = array(
      'title' => t('Operations'),
      'help' => t('Provide a list of operation links to manage shifts.'),
      'field' => array(
        'handler' => 'shift_views_handler_field_operations',
        'click sortable' => FALSE,
      ),
    );

    $data['shift']['uid']['field']['handler'] = 'shift_views_handler_field_uid';
    $data['shift']['duration']['field']['handler'] = 'shift_views_handler_field_duration';

    return $data;
  }
}

/**
 * Implements hook_views_data().
 *
 * See how much work it is to do this instead of just using our Views Controller?
 */
function shift_views_data() {
  
  $data['shift_revision']['table']['group'] = t('Shift revision');
  $data['shift_revision']['table']['join'] = array(
    'shift' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  $data['shift_revision']['vid'] = array(
    'title' => t('Revision Id'),
    'help' => t('The revision identifier for this shift revision.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['shift_revision']['changed'] = array(
    'title' => t('Revision modified date'),
    'help' => t('The date and time this shift revision was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['shift_revision']['changedby'] = array(
    'title' => t('Revision modified by'),
    'help' => t('The user who created this shift revision.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Changed by'),
      'base' => 'users',
      'base field' => 'uid',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['shift_revision']['uid'] = array(
    'title' => t('Revision shift user'),
    'help' => t('The user associated with this shift.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
      'base' => 'users',
      'base field' => 'uid',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['shift_revision']['start'] = array(
    'title' => t('Revision shift start'),
    'help' => t('The start time and date of this shift revision.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['shift_revision']['stop'] = array(
    'title' => t('Revision shift stop'),
    'help' => t('The stop time and date of this shift revision.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['shift_revision']['duration'] = array(
    'title' => t('Revision shift duration'),
    'help' => t('The duration of this shift revision.'),
    'field' => array(
      'handler' => 'shift_views_handler_field_duration',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
