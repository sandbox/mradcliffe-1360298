<?php
/**
 * @file
 *   Shift uid handler
 */


/**
 * @class shift_views_handler_field_uid
 */
class shift_views_handler_field_uid extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    $options['link'] = array('default' => 1, 'translatable' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['link'] = array(
      '#type' => 'radios',
      '#title' => t('Link to user shifts page?'),
      '#options' => array(0 => t('No'), 1 => t('Yes')),
      '#default_value' => $this->options['link'],
      '#required' => TRUE,
    );

    parent::options_form($form, $form_state);
  }

  function render($values) {
    $value = $this->get_value($values);

    if ($this->options['link']) {
      return l(t('User Shifts'), 'user/' . $value . '/shifts');
    }

    return $value;
  }

}
