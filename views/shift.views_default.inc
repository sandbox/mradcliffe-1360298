<?php
/**
 * @file
 *   Default views.
 */
  
  
/**
 * Implements hook_views_default_views().
 */
function shift_views_default_views() {
  $views = array();
  
  $view = new view;
  $view->name = 'user_shifts';
  $view->description = 'Display a time card like interface';
  $view->tag = 'default';
  $view->base_table = 'shift';
  $view->human_name = 'User Shifts';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User Shifts';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'manage shifts';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'type' => 'type',
    'start' => 'start',
    'stop' => 'stop',
    'duration' => 'duration',
  );
  $handler->display->display_options['style_options']['default'] = 'start';
  $handler->display->display_options['style_options']['info'] = array(
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'start' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'stop' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'duration' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Relationship: Shift: User Id */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'shift';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = 1;
  /* Field: Shift: Shift type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'shift';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 0;
  /* Field: Shift: Shift start */
  $handler->display->display_options['fields']['start']['id'] = 'start';
  $handler->display->display_options['fields']['start']['table'] = 'shift';
  $handler->display->display_options['fields']['start']['field'] = 'start';
  $handler->display->display_options['fields']['start']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['start']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['start']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['start']['alter']['external'] = 0;
  $handler->display->display_options['fields']['start']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['start']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['start']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['start']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['start']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['start']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['start']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['start']['alter']['html'] = 0;
  $handler->display->display_options['fields']['start']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['start']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['start']['hide_empty'] = 0;
  $handler->display->display_options['fields']['start']['empty_zero'] = 0;
  $handler->display->display_options['fields']['start']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['start']['date_format'] = 'short';
  /* Field: Shift: Shift Stop */
  $handler->display->display_options['fields']['stop']['id'] = 'stop';
  $handler->display->display_options['fields']['stop']['table'] = 'shift';
  $handler->display->display_options['fields']['stop']['field'] = 'stop';
  $handler->display->display_options['fields']['stop']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['external'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['stop']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['stop']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['html'] = 0;
  $handler->display->display_options['fields']['stop']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['stop']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['stop']['hide_empty'] = 0;
  $handler->display->display_options['fields']['stop']['empty_zero'] = 0;
  $handler->display->display_options['fields']['stop']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['stop']['date_format'] = 'short';
  /* Field: Shift: Duration */
  $handler->display->display_options['fields']['duration']['id'] = 'duration';
  $handler->display->display_options['fields']['duration']['table'] = 'shift';
  $handler->display->display_options['fields']['duration']['field'] = 'duration';
  $handler->display->display_options['fields']['duration']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['external'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['duration']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['duration']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['html'] = 0;
  $handler->display->display_options['fields']['duration']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['duration']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['duration']['empty'] = 'N/A';
  $handler->display->display_options['fields']['duration']['hide_empty'] = 0;
  $handler->display->display_options['fields']['duration']['empty_zero'] = 0;
  $handler->display->display_options['fields']['duration']['hide_alter_empty'] = 0;
  /* Contextual filter: Shift: User Id */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'shift';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['uid']['exception']['value'] = '';
  $handler->display->display_options['arguments']['uid']['title_enable'] = 1;
  $handler->display->display_options['arguments']['uid']['title'] = '%1\'s Shifts';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = 1;
  $handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
  $handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';
  $handler->display->display_options['arguments']['uid']['validate_options']['restrict_roles'] = 0;
  $handler->display->display_options['arguments']['uid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['uid']['not'] = 0;
  /* Filter criterion: Shift: Shift Stop */
  $handler->display->display_options['filters']['stop']['id'] = 'stop';
  $handler->display->display_options['filters']['stop']['table'] = 'shift';
  $handler->display->display_options['filters']['stop']['field'] = 'stop';
  $handler->display->display_options['filters']['stop']['operator'] = 'not empty';
  /* Filter criterion: Shift: Shift start */
  $handler->display->display_options['filters']['start']['id'] = 'start';
  $handler->display->display_options['filters']['start']['table'] = 'shift';
  $handler->display->display_options['filters']['start']['field'] = 'start';
  $handler->display->display_options['filters']['start']['operator'] = '>=';
  $handler->display->display_options['filters']['start']['exposed'] = TRUE;
  $handler->display->display_options['filters']['start']['expose']['operator_id'] = 'start_op';
  $handler->display->display_options['filters']['start']['expose']['label'] = 'Shift started after';
  $handler->display->display_options['filters']['start']['expose']['operator'] = 'start_op';
  $handler->display->display_options['filters']['start']['expose']['identifier'] = 'start';
  $handler->display->display_options['filters']['start']['expose']['multiple'] = FALSE;
  /* Filter criterion: Shift: Shift Stop */
  $handler->display->display_options['filters']['stop_1']['id'] = 'stop_1';
  $handler->display->display_options['filters']['stop_1']['table'] = 'shift';
  $handler->display->display_options['filters']['stop_1']['field'] = 'stop';
  $handler->display->display_options['filters']['stop_1']['operator'] = '<=';
  $handler->display->display_options['filters']['stop_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['stop_1']['expose']['operator_id'] = 'stop_1_op';
  $handler->display->display_options['filters']['stop_1']['expose']['label'] = 'Shift stopped before';
  $handler->display->display_options['filters']['stop_1']['expose']['operator'] = 'stop_1_op';
  $handler->display->display_options['filters']['stop_1']['expose']['identifier'] = 'stop_1';
  $handler->display->display_options['filters']['stop_1']['expose']['multiple'] = FALSE;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user/%/shifts';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Shifts';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'user-menu';
  
  $views['user-shifts'] = $view;
  
  $view = new view;
  $view->name = 'manage_shifts';
  $view->description = 'Interface for managing shifts.';
  $view->tag = 'default';
  $view->base_table = 'shift';
  $view->human_name = 'Manage Shifts';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Manage Shifts';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name_1' => 'name_1',
    'type' => 'type',
    'start' => 'start',
    'stop' => 'stop',
    'duration' => 'duration',
    'changed' => 'changed',
    'name' => 'name',
    'operations' => 'operations',
    'uid' => 'uid',
  );
  $handler->display->display_options['style_options']['default'] = 'start';
  $handler->display->display_options['style_options']['info'] = array(
    'name_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'start' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
    ),
    'stop' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'duration' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Relationship: Shift: Last modified by */
  $handler->display->display_options['relationships']['changedby']['id'] = 'changedby';
  $handler->display->display_options['relationships']['changedby']['table'] = 'shift';
  $handler->display->display_options['relationships']['changedby']['field'] = 'changedby';
  $handler->display->display_options['relationships']['changedby']['label'] = 'Changed By';
  $handler->display->display_options['relationships']['changedby']['required'] = 1;
  /* Relationship: Shift: User Id */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'shift';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = 1;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'changedby';
  $handler->display->display_options['fields']['name_1']['label'] = 'User';
  $handler->display->display_options['fields']['name_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name_1']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['name_1']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name_1']['overwrite_anonymous'] = 0;
  /* Field: Shift: Shift type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'shift';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 0;
  /* Field: Shift: Shift start */
  $handler->display->display_options['fields']['start']['id'] = 'start';
  $handler->display->display_options['fields']['start']['table'] = 'shift';
  $handler->display->display_options['fields']['start']['field'] = 'start';
  $handler->display->display_options['fields']['start']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['start']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['start']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['start']['alter']['external'] = 0;
  $handler->display->display_options['fields']['start']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['start']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['start']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['start']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['start']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['start']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['start']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['start']['alter']['html'] = 0;
  $handler->display->display_options['fields']['start']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['start']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['start']['hide_empty'] = 0;
  $handler->display->display_options['fields']['start']['empty_zero'] = 0;
  $handler->display->display_options['fields']['start']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['start']['date_format'] = 'short';
  /* Field: Shift: Shift Stop */
  $handler->display->display_options['fields']['stop']['id'] = 'stop';
  $handler->display->display_options['fields']['stop']['table'] = 'shift';
  $handler->display->display_options['fields']['stop']['field'] = 'stop';
  $handler->display->display_options['fields']['stop']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['external'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['stop']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['stop']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['stop']['alter']['html'] = 0;
  $handler->display->display_options['fields']['stop']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['stop']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['stop']['hide_empty'] = 0;
  $handler->display->display_options['fields']['stop']['empty_zero'] = 0;
  $handler->display->display_options['fields']['stop']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['stop']['date_format'] = 'short';
  /* Field: Shift: Duration */
  $handler->display->display_options['fields']['duration']['id'] = 'duration';
  $handler->display->display_options['fields']['duration']['table'] = 'shift';
  $handler->display->display_options['fields']['duration']['field'] = 'duration';
  $handler->display->display_options['fields']['duration']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['text'] = 'N/A';
  $handler->display->display_options['fields']['duration']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['external'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['duration']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['duration']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['duration']['alter']['html'] = 0;
  $handler->display->display_options['fields']['duration']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['duration']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['duration']['empty'] = 'N/A';
  $handler->display->display_options['fields']['duration']['hide_empty'] = 0;
  $handler->display->display_options['fields']['duration']['empty_zero'] = 0;
  $handler->display->display_options['fields']['duration']['hide_alter_empty'] = 0;
  /* Field: Shift: Last modified */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'shift';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['external'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['changed']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['changed']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['changed']['alter']['html'] = 0;
  $handler->display->display_options['fields']['changed']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['changed']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['changed']['hide_empty'] = 0;
  $handler->display->display_options['fields']['changed']['empty_zero'] = 0;
  $handler->display->display_options['fields']['changed']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'changedby';
  $handler->display->display_options['fields']['name']['label'] = 'Last Modified By';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['name']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  /* Field: Shift: Operations */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'shift';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  /* Field: Shift: User Id */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'shift';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['uid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['uid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['uid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['uid']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['uid']['link'] = '1';
  /* Filter criterion: Shift: Shift start */
  $handler->display->display_options['filters']['start']['id'] = 'start';
  $handler->display->display_options['filters']['start']['table'] = 'shift';
  $handler->display->display_options['filters']['start']['field'] = 'start';
  $handler->display->display_options['filters']['start']['operator'] = '>=';
  $handler->display->display_options['filters']['start']['exposed'] = TRUE;
  $handler->display->display_options['filters']['start']['expose']['operator_id'] = 'start_op';
  $handler->display->display_options['filters']['start']['expose']['label'] = 'Shift started after';
  $handler->display->display_options['filters']['start']['expose']['operator'] = 'start_op';
  $handler->display->display_options['filters']['start']['expose']['identifier'] = 'start';
  $handler->display->display_options['filters']['start']['expose']['multiple'] = FALSE;
  /* Filter criterion: Shift: Shift Stop */
  $handler->display->display_options['filters']['stop']['id'] = 'stop';
  $handler->display->display_options['filters']['stop']['table'] = 'shift';
  $handler->display->display_options['filters']['stop']['field'] = 'stop';
  $handler->display->display_options['filters']['stop']['operator'] = '<=';
  $handler->display->display_options['filters']['stop']['exposed'] = TRUE;
  $handler->display->display_options['filters']['stop']['expose']['operator_id'] = 'stop_op';
  $handler->display->display_options['filters']['stop']['expose']['label'] = 'Shift stopped before';
  $handler->display->display_options['filters']['stop']['expose']['operator'] = 'stop_op';
  $handler->display->display_options['filters']['stop']['expose']['identifier'] = 'stop';
  $handler->display->display_options['filters']['stop']['expose']['multiple'] = FALSE;
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'User name';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['multiple'] = FALSE;
  $handler->display->display_options['filters']['uid']['expose']['reduce'] = 0;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/shift/overview';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'Overview';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Shifts';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  
  $views['manage-shifts'] = $view;
  
  return $views;
  
}
