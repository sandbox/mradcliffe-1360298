<?php
/**
 * @file
 *   Shift theme functions
 */


/**
 * Theme a shift duration in a friendly manner.
 *
 * @param $vars
 * @return string HTML
 */
function theme_shift_duration($vars) {

  $duration = $vars['duration'];
  $format = $vars['format'];

  // Setup or attributes array.
  if (!isset($vars['attributes'])) {
    $attributes = array(
      'class' => array('shift-duration'),
    );
  }
  else {
    $attributes = $vars['attributes'];
  }

  // Exit out of here with just a simple span wrapper.
  if ($format == 'raw') {
    return '<span ' . drupal_attributes($attributes) . '>' . $duration . '</span>';
  }

  // Do the actual logic for displaying seconds -> H M S
  $raw = '';

  if ($duration < 60) {
    // If we do have a shift of less than a minute we really need a friendly
    // descriptor either way.
    $sec = ($duration < 10) ? '0' . $duration : $duration;

    $raw = ($format == 'short') ? '00:00:' . $sec : format_plural($duration, '1 second', '@count seconds');
  }
  else if ($duration < 3600) {
    // We have minutes and seconds.
    $min = number_format($duration / 60, 0, '', '');
    $sec = is_float($duration %60) ? number_format($duration % 60, 0, '', '') : 0;

    if ($format == 'short') {
      $raw .= (($min < 10) ? '0' . $min : $min) . ':' . (($sec < 10) ? '0' . $sec : $sec);
    }
    else {
      $raw .= format_plural($min, '1 minute ', '@count minutes ') . format_plural($sec, '1 second', '@count seconds');
    }
  }
  else {
    // We have hours, minutes, and seconds.
    $hour = number_format($duration / 3600, 0, '', '');
    $min = is_float($duration % 3600 / 60 <> 0) ? number_format(($duration % 3600) / 60, 0, '', '') : 0;
    $sec = number_format($duration % 3600 % 60, 0, '', '');

    if ($format == 'short') {
      $hour = ($hour < 10) ? '0' . $hour : $hour;
      $min = ($min < 10) ? '0' . $min : $min;
      $sec = ($sec < 10) ? '0' . $sec : $sec;

      $raw .= $hour . ':' . $min . ':' . $sec;
    }
    else {
      $raw .= format_plural($hour, '1 hour ', '@count hours ') . format_plural($min, '1 minute ', '@count minutes ') . format_plural($sec, '1 second', '@count seconds');
    }
  }

  return '<span ' . drupal_attributes($attributes) . '>' . $raw . '</span>';
}
